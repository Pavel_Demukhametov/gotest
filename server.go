package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"
)

type UserXML struct {
	ID            int    `xml:"id"`
	GUID          string `xml:"guid"`
	IsActive      bool   `xml:"isActive"`
	Balance       string `xml:"balance"`
	Picture       string `xml:"picture"`
	Age           int    `xml:"age"`
	EyeColor      string `xml:"eyeColor"`
	FirstName     string `xml:"first_name"`
	LastName      string `xml:"last_name"`
	Gender        string `xml:"gender"`
	Company       string `xml:"company"`
	Email         string `xml:"email"`
	Phone         string `xml:"phone"`
	Address       string `xml:"address"`
	About         string `xml:"about"`
	Registered    string `xml:"registered"`
	FavoriteFruit string `xml:"favoriteFruit"`
}

type SearchParams struct {
	Query      string
	OrderField string
	OrderBy    int
	Limit      int
	Offset     int
}

var (
	Link        = "dataset.xml"
	validTokens = map[string]struct{}{"admin": {}, "admin1": {}}
)

type UsersXML struct {
	XMLName xml.Name  `xml:"root"`
	XMLData []UserXML `xml:"row"`
}

const (
	FieldID   = "Id"
	FieldName = "Name"
	FieldAge  = "Age"
)

func readXMLFile(xmlFile string) (*UsersXML, error) {
	file, err := os.Open(xmlFile)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	return DecodeFile(file)
}

func DecodeFile(reader io.Reader) (*UsersXML, error) {
	var usersXML UsersXML
	decoder := xml.NewDecoder(reader)
	err := decoder.Decode(&usersXML)
	if err != nil {
		return nil, err
	}
	return &usersXML, nil
}

func EncodeJSON(w http.ResponseWriter, data interface{}) {
	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		SendError(w, http.StatusInternalServerError, fmt.Sprintf("Failed to encode JSON: %s", err))
	}
}

func isValidAccessToken(token string) bool {
	_, ok := validTokens[token]
	return ok
}

func SendError(w http.ResponseWriter, statusCode int, message string) {
	errMes := SearchErrorResponse{Error: message}
	w.WriteHeader(statusCode)
	if err := json.NewEncoder(w).Encode(errMes); err != nil {
		http.Error(w, "Failed to encode", http.StatusInternalServerError)
		return
	}
}

func validateParams(r *http.Request) error {
	params := []string{"query", "order_field", "order_by", "limit", "offset"}
	for _, param := range params {
		if _, ok := r.URL.Query()[param]; !ok {
			return fmt.Errorf("all parameters ('%s') must be present in the request", strings.Join(params, "', '"))
		}
	}
	return nil
}

func parseParams(r *http.Request) (*SearchParams, error) {
	params := &SearchParams{}

	params.Query = r.URL.Query().Get("query")
	params.OrderField = r.URL.Query().Get("order_field")
	if params.OrderField == "" {
		params.OrderField = FieldName
	}
	if params.OrderField != FieldID && params.OrderField != FieldAge && params.OrderField != FieldName {
		return nil, fmt.Errorf("OrderField invalid")
	}

	orderByStr := r.URL.Query().Get("order_by")
	var err error
	params.OrderBy, err = strconv.Atoi(orderByStr) // Corrected here
	if err != nil {
		return nil, fmt.Errorf("order_by must be a number")
	}
	if params.OrderBy != 0 && params.OrderBy != 1 && params.OrderBy != -1 {
		return nil, fmt.Errorf("OrderBy invalid")
	}

	limitStr := r.URL.Query().Get("limit")
	params.Limit, err = strconv.Atoi(limitStr)
	if err != nil || params.Limit < 0 {
		return nil, fmt.Errorf("limit must be a positive number")
	}
	if params.Limit > 25 {
		params.Limit = 25
	}

	offsetStr := r.URL.Query().Get("offset")
	params.Offset, err = strconv.Atoi(offsetStr)
	if err != nil || params.Offset < 0 {
		return nil, fmt.Errorf("offset must be a positive number")
	}

	return params, nil
}

func SearchServer(w http.ResponseWriter, r *http.Request) {
	users, err := readXMLFile(Link)
	if err != nil {
		SendError(w, http.StatusInternalServerError, fmt.Sprintf("Failed to read XML: %s", err))
		return
	}

	if !isValidAccessToken(r.Header.Get("AccessToken")) {
		SendError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}

	if err = validateParams(r); err != nil {
		SendError(w, http.StatusBadRequest, err.Error())
		return
	}

	params, err := parseParams(r)
	if err != nil {
		SendError(w, http.StatusBadRequest, err.Error())
		return
	}

	var filteredUsers []UserXML
	for _, user := range users.XMLData {
		if params.Query == "" || strings.Contains(user.FirstName+user.LastName, params.Query) || strings.Contains(user.About, params.Query) {
			filteredUsers = append(filteredUsers, user)
		}
	}

	sortUsersByField(filteredUsers, params.OrderField, params.OrderBy)
	startIndex := params.Offset
	endIndex := params.Offset + params.Limit
	if startIndex >= len(filteredUsers) {
		startIndex = len(filteredUsers)
	}
	if endIndex > len(filteredUsers) {
		endIndex = len(filteredUsers)
	}
	usersToReturn := filteredUsers[startIndex:endIndex]

	usersRes := make([]User, len(usersToReturn))
	for i, u := range usersToReturn {
		usersRes[i] = User{
			ID:     u.ID,
			Name:   u.FirstName + " " + u.LastName,
			Age:    u.Age,
			About:  u.About,
			Gender: u.Gender,
		}
	}

	EncodeJSON(w, usersRes)
}

func sortUsersByField(users []UserXML, orderField string, orderBy int) {

	if orderBy == 0 {
		return
	}

	switch orderField {
	case FieldID:
		sort.Slice(users, func(i, j int) bool {
			if orderBy == -1 {
				return users[i].ID > users[j].ID
			}
			return users[i].ID < users[j].ID
		})
	case FieldAge:
		sort.Slice(users, func(i, j int) bool {
			if orderBy == -1 {
				return users[i].Age > users[j].Age
			}
			return users[i].Age < users[j].Age
		})
	case FieldName:
		sort.Slice(users, func(i, j int) bool {
			if orderBy == -1 {
				return (users[i].FirstName + users[i].LastName) > (users[j].FirstName + users[j].LastName)
			}
			return (users[i].FirstName + users[i].LastName) < (users[j].FirstName + users[j].LastName)
		})
	}
}

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
)

type ErrorWriter struct{}

func (ew *ErrorWriter) Header() http.Header {
	return http.Header{}
}

func (ew *ErrorWriter) Write(data []byte) (int, error) {
	return 0, errors.New("write error")
}

func (ew *ErrorWriter) WriteHeader(statusCode int) {}

func (srv *SearchClient) ClientTest(params map[string]string) (*SearchResponse, error) {
	searcherParams := url.Values{}
	for key, value := range params {
		searcherParams.Add(key, value)
	}

	searcherReq, err := http.NewRequest("GET", srv.URL+"?"+searcherParams.Encode(), nil)
	if err != nil {
		return nil, err
	}
	searcherReq.Header.Add("AccessToken", srv.AccessToken)

	resp, err := client.Do(searcherReq)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		errResp := SearchErrorResponse{}
		err = json.Unmarshal(body, &errResp)
		if err != nil {
			return nil, fmt.Errorf("can't unpack error json: %s", err)
		}
		return nil, fmt.Errorf(errResp.Error)
	}

	data := []User{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		return nil, fmt.Errorf("can't unpack result json: %s", err)
	}

	result := SearchResponse{
		Users: data,
	}

	return &result, err
}

func TestBadClient(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	defer ts.Close()

	client := &SearchClient{AccessToken: "admin", URL: ts.URL}

	tests := []struct {
		name   string
		params map[string]string
		err    string
	}{
		{
			name:   "Error Param Test",
			params: map[string]string{"errParam": "test"},
			err:    "all parameters",
		},
		{
			name:   "Invalid OrderBy Test",
			params: map[string]string{"query": "test", "order_field": "Name", "order_by": "invalid", "limit": "1", "offset": "0"},
			err:    "order_by must be a number",
		},
		{
			name:   "Negative Limit Test",
			params: map[string]string{"query": "test", "order_field": "Name", "order_by": "1", "limit": "-1", "offset": "0"},
			err:    "limit must be a positive number",
		},
		{
			name:   "Invalid Limit Test",
			params: map[string]string{"query": "test", "order_field": "Name", "order_by": "1", "limit": "invalid", "offset": "0"},
			err:    "limit must be a positive number",
		},
		{
			name:   "Negative Offset Test",
			params: map[string]string{"query": "test", "order_field": "Name", "order_by": "1", "limit": "1", "offset": "-1"},
			err:    "offset must be a positive number",
		},
		{
			name:   "Invalid Offset Test",
			params: map[string]string{"query": "test", "order_field": "Name", "order_by": "1", "limit": "1", "offset": "invalid"},
			err:    "offset must be a positive number",
		},
	}

	for _, teastCase := range tests {
		t.Run(teastCase.name, func(t *testing.T) {
			_, err := client.ClientTest(teastCase.params)
			if err == nil || !strings.Contains(err.Error(), teastCase.err) {
				t.Errorf("expected error containing %q but got %v", teastCase.err, err)
			}
		})
	}
}

func TestInvalidXML(t *testing.T) {
	invalidXMLData := `<root><row><id>1</id><invalid></row></root>`
	reader := strings.NewReader(invalidXMLData)
	_, err := DecodeFile(reader)
	if err == nil {
		t.Fatal("Expected an error, got none")
	}

	if !strings.Contains(err.Error(), "XML syntax error") {
		t.Fatalf("Expected XML syntax error, got %v", err)
	}
}

func TestSendErrorWithEncode(t *testing.T) {
	w := &ErrorWriter{}
	SendError(w, http.StatusBadRequest, "Test error")
}

func TestEncodeJSON(t *testing.T) {
	type unmarshalableType struct {
		Func func()
	}

	rec := httptest.NewRecorder()
	data := unmarshalableType{}
	expectedError := "Failed to encode JSON:"

	EncodeJSON(rec, data)

	resp := rec.Result()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("Failed to read response body: %v", err)
	}

	bodyStr := strings.Trim(string(body), "\n")

	var errorResp SearchErrorResponse
	err = json.Unmarshal(body, &errorResp)
	if err != nil {
		t.Fatalf("Failed to unmarshal response body: %v", err)
	}

	if resp.StatusCode != http.StatusInternalServerError || !strings.Contains(errorResp.Error, expectedError) {
		t.Errorf("expected error containing %q but got %v", expectedError, bodyStr)
	}
}

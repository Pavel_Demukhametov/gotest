// client_test.go
package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

func NewSearchRequest(limit, offset int, query, orderField string, orderBy int) SearchRequest {
	return SearchRequest{
		Limit:      limit,
		Offset:     offset,
		Query:      query,
		OrderField: orderField,
		OrderBy:    orderBy,
	}
}

func TestFindUsersSuccess(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	defer ts.Close()

	client := &SearchClient{AccessToken: "admin", URL: ts.URL}

	var successCasesRequests = []SearchRequest{
		NewSearchRequest(1, 0, "", "Age", OrderByAsc),
		NewSearchRequest(1, 0, "", "Name", OrderByAsc),
		NewSearchRequest(1, 0, "", "Id", OrderByAsc),
		NewSearchRequest(10, 0, "", "Id", OrderByAsc),
		NewSearchRequest(10, 5, "", "Id", OrderByAsc),
		NewSearchRequest(10, 5, "", "Id", orderAsc),
		NewSearchRequest(0, 0, "", "Age", OrderByDesc),
		NewSearchRequest(1, 0, "", "Name", OrderByDesc),
		NewSearchRequest(1, 0, "", "Id", OrderByDesc),
		NewSearchRequest(10, 5, "", "Id", OrderByAsIs),
		NewSearchRequest(100, 0, "", "Id", OrderByAsIs),
		NewSearchRequest(100, 0, "Nulla", "Id", OrderByAsIs),
		NewSearchRequest(100, 0, "SomeWrongQuery1", "Id", OrderByAsIs),
		NewSearchRequest(100, 0, "", "", OrderByAsIs),
	}

	for _, request := range successCasesRequests {
		_, err := client.FindUsers(request)
		if err != nil {
			t.Errorf("Expected no error, but got %v", err)
		}
	}
}

func TestFindUsersError(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	defer ts.Close()

	testCases := []struct {
		name             string
		client           *SearchClient
		request          SearchRequest
		expectedError    string
		expectedResponse *SearchResponse
	}{
		{
			name:          "Unauthorized",
			client:        &SearchClient{AccessToken: "invalid token", URL: ts.URL},
			request:       NewSearchRequest(1, 0, "", "Name", OrderByAsc),
			expectedError: "bad AccessToken",
		},
		{
			name:          "Bad Request - Invalid OrderField",
			client:        &SearchClient{AccessToken: "admin", URL: ts.URL},
			request:       NewSearchRequest(1, 0, "", "badField", OrderByAsc),
			expectedError: "OrderFeld",
		},
		{
			name:          "Bad Request - Invalid Limit",
			client:        &SearchClient{AccessToken: "admin", URL: ts.URL},
			request:       NewSearchRequest(-1, 0, "", "", OrderByAsc),
			expectedError: "limit must be > 0",
		},
		{
			name:          "Bad Request - Invalid Offset",
			client:        &SearchClient{AccessToken: "admin", URL: ts.URL},
			request:       NewSearchRequest(1, -1, "", "", OrderByAsc),
			expectedError: "offset must be > 0",
		},
		{
			name:          "Bad Request - Invalid OrderBy",
			client:        &SearchClient{AccessToken: "admin", URL: ts.URL},
			request:       NewSearchRequest(1, 0, "", "", 10000),
			expectedError: "bad request error",
		},
		{
			name:          "Unknown Error",
			client:        &SearchClient{AccessToken: "admin", URL: "http://127.0.0.1:12345"},
			request:       NewSearchRequest(1, 0, "", "Name", OrderByAsc),
			expectedError: "unknown error",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			response, err := tc.client.FindUsers(tc.request)
			if err != nil && tc.expectedError == "" {
				t.Errorf("Expected no error, but got %v", err)
			}
			if err == nil && tc.expectedError != "" {
				t.Errorf("Expected error %v, but got none", tc.expectedError)
			}
			if err != nil && !strings.Contains(err.Error(), tc.expectedError) {
				t.Errorf("Expected error to contain %v, but got %v", tc.expectedError, err)
			}
			if response != tc.expectedResponse {
				t.Errorf("Expected response %v, but got %v", tc.expectedResponse, response)
			}
		})
	}
}

func TestFindUsersCustomServerError(t *testing.T) {
	testCases := []struct {
		name             string
		serverFunc       func(w http.ResponseWriter, r *http.Request)
		client           *SearchClient
		request          SearchRequest
		expectedError    string
		expectedResponse *SearchResponse
	}{
		{
			name: "Timeout",
			serverFunc: func(w http.ResponseWriter, r *http.Request) {
				time.Sleep(2 * time.Second)
			},
			client:        &SearchClient{AccessToken: "admin"},
			request:       NewSearchRequest(1, 0, "", "Name", OrderByAsc),
			expectedError: "timeout for",
		},
		{
			name: "Invalid JSON in Response Body",
			serverFunc: func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprint(w, `{"Invalid JSON}`)
			},
			client:        &SearchClient{AccessToken: "admin"},
			request:       SearchRequest{Limit: 1, Offset: 0, OrderField: "Name", OrderBy: 1},
			expectedError: "cant unpack result json",
		},
		{
			name: "Invalid JSON in Error Response",
			serverFunc: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusBadRequest)
				fmt.Fprint(w, `{"Invalid JSON}`)
			},
			client:        &SearchClient{AccessToken: "admin"},
			request:       SearchRequest{Limit: 1, Offset: 0, OrderField: "Name", OrderBy: 1},
			expectedError: "cant unpack error json",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ts := httptest.NewServer(http.HandlerFunc(tc.serverFunc))
			defer ts.Close()

			tc.client.URL = ts.URL
			response, err := tc.client.FindUsers(tc.request)
			if err != nil && tc.expectedError == "" {
				t.Errorf("Expected no error, but got %v", err)
			}
			if err == nil && tc.expectedError != "" {
				t.Errorf("Expected error %v, but got none", tc.expectedError)
			}
			if err != nil && !strings.Contains(err.Error(), tc.expectedError) {
				t.Errorf("Expected error to contain %v, but got %v", tc.expectedError, err)
			}
			if response != tc.expectedResponse {
				t.Errorf("Expected response %v, but got %v", tc.expectedResponse, response)
			}
		})
	}
}

func TestServerBadDataSourse(t *testing.T) {
	originalLink := Link
	defer func() { Link = originalLink }()

	Link = "wrongLink"
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	defer ts.Close()

	client := &SearchClient{AccessToken: "admin", URL: ts.URL}

	var errorCasesBadRequests = []SearchRequest{
		NewSearchRequest(1, 0, "", "Age", OrderByAsc),
	}

	for _, badRequest := range errorCasesBadRequests {
		response, err := client.FindUsers(badRequest)
		if err == nil {
			t.Errorf("expected an error but got none")
		}
		if response != nil {
			t.Errorf("Expected response to be nil, but got %v", response)
		}
	}
}
